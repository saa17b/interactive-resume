# Sebastian Arcila, IT #

### I am pursuing a B.S. in Information Technology with a focus on Java and Web App Development. As a senior graduating in April, I am looking to start a full-time job in May.

### I am currently located in Tallahassee, Florida, but I am willing to relocate for work.

*Below you will find current and past projects I have developed during my time at Florida State.*

1. [LIS4381: Web Applications Development](https://bitbucket.org/saa17b/lis4381/src/master/)
    - Course Description:
        - This course focuses on concepts and best practices for managing mobile technology projects. It covers processes and requirements for developing mobile applications and principles for effective interface and user experience design. Students develop a prototype of a mobile app and prepare a proposal and other documentation for communicating contractual and functional specifications to clients. Students also examine different issues and concerns that may influence the wide-spread adoption and implementation of mobile applications.
    - Course Objectives:
        - Develop “mobile first” applications using HTML, CSS, JavaScript, jQuery, XML, Java skills, client-side as well as server-side skills, and data repository interfaces
        - Use industry standard design architectures when creating mobile applications, Utilize current development tools and APIs to build mobile applications, and Review and analyze mobile hardware and software limitations based upon current literature

2. [LIS4368: Advanced Web Applications Development](https://bitbucket.org/saa17b/lis4368/src/master/)
    - Course Description:
        - This course provides a foundation in developing web applications with an emphasis on server-side concepts, tools and methods. Topics include basic web application programming, advanced objectoriented (OOP) and web application development. Students enrolled in this course will develop basic programming skills in a modern web development environment, understand web application development principles and be able to find and use web application development resources on the Internet.
    - Course Objectives:
        - Demonstrate the ability to program and deploy client/server-side scripts 
        - Describe web-based input and output processes
        - Demonstrate web application to data source connectivity
        - Develop a dynamic web application using a mix of front-end and back-end web technologies
        - Employ OOP techniques, as well as business logic using a strongly typed language.
        - Create client- and server-side data validation.

3. [C# Gradebook Application](https://bitbucket.org/saa17b/gradebook-application/src/master/)
    - Course Description:
        - This course, C# Fundamentals, will help you be comfortable with fundamental programming concepts on any platform. First, you will learn about the syntax of the C# language. Next, you will discover the built-in features of .NET. Finally, you will explore how to solve problems using object-oriented programming techniques. When you are finished with this course, you will have the skills and knowledge you need for real-world solutions.
        - [Click here](https://www.pluralsight.com/courses/csharp-fundamentals-dev) to visit course.
        
4. [Angular CRUD Application](https://bitbucket.org/saa17b/angular-application/src/master/)
    - Course Description:
        - In this course, you will learn how to create great web apps and stay up to date on the latest app development technologies, by coming up to speed quickly with Angular's components, templates, and services. You will get there by learning major topics like to set up your environment, learning about components, templates, and data binding and how they work together, discover how to build clean components with strongly-typed code, as well as building nested components and how to use dependency injection to inject the services you build and how to retrieve data using HTTP, navigation and routing.
        - [Click here](https://www.pluralsight.com/courses/angular-2-getting-started-update) to visit course.